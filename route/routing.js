app.get('/firstRoute', tokenValidator, (req, res) => {
    res.send("First trial of Routing");
});

app.post('/postExample', tokenValidator, (req, res) => {
    res.send("First Post Route");
})