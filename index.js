const express = require('express');
const tokenValidator = require('./middleware/tokenValidator');

const app = express();

app.get('/', tokenValidator, (req, res) => {
    res.send("First trial of Express");
});

app.listen(4000, () => {
    console.log("Listening to Port 4000");
})